export EDITOR="nvim"
export PAGER="less"
export TERMINAL="alacritty"
export SHELL="/usr/bin/zsh"
