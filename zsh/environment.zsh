# Git Interface, needed because it doesn't auto search for themes
export GITUI_CONF_DIR="$HOME/.config/gitui"

# Terminal multiplexer, just making sure it doesn't try looking in $HOME
export ZELLIJ_LAYOUTS="$HOME/.config/zellij/layouts"
export ZELLIJ_THEMES="$HOME/.config/zellij/themes"
