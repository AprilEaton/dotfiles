# Replace ls with a prettier ls
alias ls="exa -HlghlS --git --icons --time=modified"
alias lls-"exa -aghHlRT --git --icons --time=modified --level=3"

# GitUI, please just add a proper config file
alias gitui="gitui -t $GITUI_CONF_DIR/catppuccin-macchiato.ron"

# Common directories to jump to
alias cdc="cd /filedump/Coding_Projects"
alias cds="cd /filedump/School_Things"
alias cdh="cd ~"
alias cdd="cd ~/dotfiles"
