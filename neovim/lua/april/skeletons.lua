vim.api.nvim_create_autocmd({"BufNewFile"}, {
    group = vim.api.nvim_create_augroup("create_skeletons", { clear = true }),
    pattern = "*.desktop",
    command = "0r ~/.config/nvim/skeletons/skeleton.desktop"
})
