vim.g.mkdp_auto_start = 0
vim.g.mkdp_auto_close = 0
vim.g.mkdp_refresh_slow = 1
vim.g.mkdp_command_for_global = 0
vim.g.mkdp_open_to_the_world = 0
vim.g.mkdp_echo_preview_url = 1
vim.g.mkdp_page_title = ' ${name} Preview '
vim.g.mkdp_theme = 'dark'

vim.keymap.set('n', '<Leader>mp', vim.cmd.MarkdownPreviewToggle, { noremap = true })
