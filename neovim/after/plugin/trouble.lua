vim.keymap.set(
    "n", "<leader>dw",
    "<cmd>TroubleToggle workspace_diagnostics<cr>",
    { silent = true, noremap = true }
)

vim.keymap.set(
    "n", "<leader>dd",
    "<cmd>TroubleToggle document_diagnostics<cr>",
    { silent = true, noremap = true }
)

vim.keymap.set(
    "n", "<leader>xq",
    "<cmd>TroubleToggle quickfix<cr>",
    { silent = true, noremap = true }
)
